import persistence.JDBCImplementation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class App {

    public static void main(String[] args) {
        JDBCImplementation jdbc = new JDBCImplementation();
        Connection connection = jdbc.getConnection();

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(
                    "SELECT * FROM rankos"
            );
            while(resultSet.next()) {
                System.out.print(resultSet.getInt("id") + " ");
                System.out.print(resultSet.getString("vardas") + " ");
                System.out.print(resultSet.getString("pavarde") + " ");
                System.out.print(resultSet.getInt("amzius") + " ");
                System.out.println();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            connection.createStatement().execute(
                    "UPDATE rankos SET amzius=amzius+50 WHERE vardas like '%om%' "
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println();


        try {
            ResultSet resultSet = connection.createStatement().executeQuery(
                    "SELECT * FROM rankos"
            );
            while(resultSet.next()) {
                System.out.print(resultSet.getInt("id") + " ");
                System.out.print(resultSet.getString("vardas") + " ");
                System.out.print(resultSet.getString("pavarde") + " ");
                System.out.print(resultSet.getInt("amzius") + " ");
                System.out.println();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
